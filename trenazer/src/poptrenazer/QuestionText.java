/*    */ package poptrenazer;
/*    */ 
/*    */ public class QuestionText
/*    */ {
/*    */   private boolean isTrue;
/*    */   private String questionText;
/*    */ 
/*    */   public QuestionText(boolean isTrue, String questionText)
/*    */   {
/* 18 */     this.isTrue = isTrue;
/* 19 */     this.questionText = questionText;
/*    */   }
/*    */ 
/*    */   public String getQuestionText() {
/* 23 */     return this.questionText;
/*    */   }
/*    */ 
/*    */   public void setQuestionText(String questionText) {
/* 27 */     this.questionText = questionText;
/*    */   }
/*    */ 
/*    */   public boolean isTrue()
/*    */   {
/* 33 */     return this.isTrue;
/*    */   }
/*    */ 
/*    */   public void setIsTrue(boolean isTrue) {
/* 37 */     this.isTrue = isTrue;
/*    */   }
/*    */ }

/* Location:           /media/Data/Skola/5.rocnik/LS/PaOP/trenazer/kamsv.jar
 * Qualified Name:     sk.tuke.student.kamsv.tester.QuestionText
 * JD-Core Version:    0.6.2
 */