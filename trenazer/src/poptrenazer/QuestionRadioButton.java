/*    */ package poptrenazer;
/*    */ 
import javax.swing.JCheckBox;
/*    */ import javax.swing.JRadioButton;
/*    */ 
/*    */ public class QuestionRadioButton extends JCheckBox
/*    */ {
/*    */   private static final long serialVersionUID = 5137796363726297283L;
/*    */   private boolean isTrue;
/*    */ 
/*    */   public QuestionRadioButton(String string, boolean isTrue)
/*    */   {
/* 23 */     super(string);
/* 24 */     this.isTrue = isTrue;
/*    */   }
/*    */ 
/*    */   public boolean isTrue() {
/* 28 */     return this.isTrue;
/*    */   }
/*    */ 
/*    */   public void setIsTrue(boolean isTrue) {
/* 32 */     this.isTrue = isTrue;
/*    */   }

/*    */ }

/* Location:           /media/Data/Skola/5.rocnik/LS/PaOP/trenazer/kamsv.jar
 * Qualified Name:     sk.tuke.student.kamsv.tester.QuestionRadioButton
 * JD-Core Version:    0.6.2
 */