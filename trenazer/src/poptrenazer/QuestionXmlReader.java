/*    */ package poptrenazer;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.parsers.DocumentBuilder;
/*    */ import javax.xml.parsers.DocumentBuilderFactory;
/*    */ import javax.xml.parsers.ParserConfigurationException;
/*    */ import org.w3c.dom.Document;
/*    */ import org.w3c.dom.Element;
/*    */ import org.w3c.dom.NamedNodeMap;
/*    */ import org.w3c.dom.Node;
/*    */ import org.w3c.dom.NodeList;
/*    */ import org.xml.sax.SAXException;
/*    */ 
/*    */ public class QuestionXmlReader
/*    */ {
/* 27 */   private final String FILE_LOCATION = "KaMs.xml";
/* 28 */   private List<Question> questions = new ArrayList();
/*    */ 
/*    */   public List<Question> questions() throws ParserConfigurationException, SAXException, IOException {
/* 31 */     File fXmlFile = new File("KaMs.xml");
/* 32 */     DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
/* 33 */     DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
/* 34 */     Document doc = dBuilder.parse(fXmlFile);
/*    */ 
/* 36 */     doc.getDocumentElement().normalize();
/* 37 */     NodeList nList = doc.getElementsByTagName("otazka");
/* 38 */     for (int i = 0; i < nList.getLength(); i++) {
/* 39 */       Node nNode = nList.item(i);
/* 40 */       if (nNode.getNodeType() == 1) {
/* 41 */         Element eElement = (Element)nNode;
/* 42 */         List options = new ArrayList();
/* 43 */         if (eElement.getElementsByTagName("option1").item(0).getAttributes().getLength() > 0)
/* 44 */           options.add(new QuestionText(true, eElement.getElementsByTagName("option1").item(0).getTextContent()));
/*    */         else {
/* 46 */           options.add(new QuestionText(false, eElement.getElementsByTagName("option1").item(0).getTextContent()));
/*    */         }
/* 48 */         if (eElement.getElementsByTagName("option2").item(0).getAttributes().getLength() > 0)
/* 49 */           options.add(new QuestionText(true, eElement.getElementsByTagName("option2").item(0).getTextContent()));
/*    */         else {
/* 51 */           options.add(new QuestionText(false, eElement.getElementsByTagName("option2").item(0).getTextContent()));
/*    */         }
				if (eElement.getElementsByTagName("option3").getLength() > 0) {
/* 53 */         if (eElement.getElementsByTagName("option3").item(0).getAttributes().getLength() > 0)
/* 54 */           options.add(new QuestionText(true, eElement.getElementsByTagName("option3").item(0).getTextContent()));
/*    */         else {
/* 56 */           options.add(new QuestionText(false, eElement.getElementsByTagName("option3").item(0).getTextContent()));
/*    */         }
				}
/* 58 */         if (eElement.getElementsByTagName("option4").getLength() > 0) {
/* 59 */           if (eElement.getElementsByTagName("option4").item(0).getAttributes().getLength() > 0)
/* 60 */             options.add(new QuestionText(true, eElement.getElementsByTagName("option4").item(0).getTextContent()));
/*    */           else {
/* 62 */             options.add(new QuestionText(false, eElement.getElementsByTagName("option4").item(0).getTextContent()));
/*    */           }
/*    */         }
/* 65 */         if (eElement.getElementsByTagName("option5").getLength() > 0) {
/* 66 */           if (eElement.getElementsByTagName("option4").item(0).getAttributes().getLength() > 0)
/* 67 */             options.add(new QuestionText(true, eElement.getElementsByTagName("option4").item(0).getTextContent()));
/*    */           else {
/* 69 */             options.add(new QuestionText(false, eElement.getElementsByTagName("option4").item(0).getTextContent()));
/*    */           }
/*    */         }
/*    */ 
/* 73 */         Question quest = new Question(eElement.getElementsByTagName("questionNumber").item(0).getTextContent(), eElement.getElementsByTagName("questionText").item(0).getTextContent(), options);
/* 74 */         this.questions.add(quest);
/*    */       }
/*    */ 
/*    */     }
/*    */ 
/* 79 */     return this.questions;
/*    */   }
/*    */ }

/* Location:           /media/Data/Skola/5.rocnik/LS/PaOP/trenazer/kamsv.jar
 * Qualified Name:     sk.tuke.student.kamsv.tester.QuestionXmlReader
 * JD-Core Version:    0.6.2
 */