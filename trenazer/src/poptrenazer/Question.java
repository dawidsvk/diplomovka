/*    */ package poptrenazer;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ public class Question
/*    */ {
/*    */   private String questionNumber;
/*    */   private String questionText;
/*    */   private List<QuestionText> questionOptions;
/*    */ 
/*    */   public Question(String questionNumber, String questionText, List<QuestionText> questionOptions)
/*    */   {
/* 21 */     this.questionNumber = questionNumber;
/* 22 */     this.questionText = questionText;
/* 23 */     this.questionOptions = questionOptions;
/*    */   }
/*    */ 
/*    */   public String getQuestionNumber() {
/* 27 */     return this.questionNumber;
/*    */   }
/*    */ 
/*    */   public String getQuestionText() {
/* 31 */     return this.questionText;
/*    */   }
/*    */ 
/*    */   public List<QuestionText> getQuestionOptions() {
/* 35 */     return this.questionOptions;
/*    */   }
/*    */ 
/*    */   public void setQuestionNumber(String questionNumber) {
/* 39 */     this.questionNumber = questionNumber;
/*    */   }
/*    */ 
/*    */   public void setQuestionText(String questionText) {
/* 43 */     this.questionText = questionText;
/*    */   }
/*    */ 
/*    */   public void setQuestionOptions(List<QuestionText> questionOptions) {
/* 47 */     this.questionOptions = questionOptions;
/*    */   }
/*    */ }

/* Location:           /media/Data/Skola/5.rocnik/LS/PaOP/trenazer/kamsv.jar
 * Qualified Name:     sk.tuke.student.kamsv.tester.Question
 * JD-Core Version:    0.6.2
 */