/*     */ package poptrenazer;
/*     */ 
/*     */ import java.awt.BorderLayout;
import java.awt.Color;
/*     */ import java.awt.GridBagLayout;
/*     */ import java.awt.GridLayout;
/*     */ import java.awt.HeadlessException;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.List;

/*     */ import javax.swing.AbstractButton;
/*     */ import javax.swing.BorderFactory;
/*     */ import javax.swing.BoxLayout;
/*     */ import javax.swing.ButtonGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JRadioButton;
/*     */ import javax.swing.border.Border;
/*     */ import javax.xml.parsers.ParserConfigurationException;

/*     */ import org.xml.sax.SAXException;
/*     */ 
/*     */ public class POPTrenazer extends JFrame
/*     */   implements ActionListener
/*     */ {
/*     */   private static final long serialVersionUID = -2707558494777715399L;
/*     */   private JPanel questionTop;
/*     */   private JPanel questionTextPanel;
/*     */   private JLabel questionText;
/*     */   private JPanel questionOptions;
/*     */   private JPanel bottomPanel;
/*     */   private JPanel newButtonPanel;
/*     */   private JPanel statusPanel;
/*     */   private JLabel statusLabel;
/*     */   private JButton nextButton;
			private JButton correctButton;
/*  53 */   private static int counter = 0;
/*  54 */   private static double correct = 0;
/*  54 */   private static double correctInLastQuestion = 0;
/*  55 */   private static int wrong = 0;
/*     */   private boolean selected;
/*  58 */   List<Question> questions = new QuestionXmlReader().questions();
/*     */ 
/* 169 */   ActionListener sliceActionListener = new ActionListener() {
/*     */     public void actionPerformed(ActionEvent actionEvent) {
/* 171 */       AbstractButton aButton = (AbstractButton)actionEvent.getSource();
/* 172 */       POPTrenazer.this.nextButton.setEnabled(true);
/* 173 */       if (((QuestionRadioButton)aButton).isTrue())
/* 174 */         POPTrenazer.this.selected = true;
/*     */       else
/* 176 */         POPTrenazer.this.selected = false;
/*     */     }
/* 169 */   };
/*     */ 
/*     */   public POPTrenazer(String string)
/*     */     throws HeadlessException, ParserConfigurationException, SAXException, IOException
/*     */   {
/*  62 */     super(string);
/*  63 */     Collections.shuffle(this.questions);
/*  64 */     Border topBorder = BorderFactory.createTitledBorder(((Question)this.questions.get(
/*  65 */       counter)).getQuestionNumber());
/*  66 */     Border options = BorderFactory.createTitledBorder("Options");
/*  67 */     Border next = BorderFactory.createTitledBorder("Proceed");
/*     */ 
/*  69 */     this.questionTextPanel = new JPanel(new BorderLayout());
/*  70 */     this.questionTextPanel.setMaximumSize(this.questionTextPanel.getPreferredSize());
/*  71 */     this.questionOptions = new JPanel();
/*  72 */     this.questionOptions.setLayout(new BoxLayout(this.questionOptions, 
/*  73 */       1));
/*  74 */     this.bottomPanel = new JPanel(new GridLayout(0, 2));
/*  75 */     this.newButtonPanel = new JPanel();
/*  76 */     this.nextButton = new JButton("Next");
/*  77 */     this.nextButton.setEnabled(false);
				this.correctButton = new JButton("Correct");
/*  77 */     this.correctButton.setEnabled(true);
/*  78 */     this.questionTop = new JPanel();
/*  79 */     this.questionTop.setLayout(new BorderLayout());
/*  80 */     this.questionTop.setBorder(topBorder);
/*  81 */     this.questionTop.add(this.questionTextPanel, "Center");
/*     */ 
/*  83 */     this.questionText = new JLabel(((Question)this.questions.get(counter)).getQuestionText());
/*     */ 
/*  85 */     this.questionTextPanel.add(this.questionText, "Center");
/*  86 */     checkBoxButtonPanel(counter);
/*  87 */     this.questionOptions.setBorder(options);
/*     */ 
/*  89 */     this.newButtonPanel.setBorder(next);
/*  90 */     this.newButtonPanel.add(this.nextButton);
				this.newButtonPanel.add(this.correctButton);
/*  91 */     this.bottomPanel.add(this.newButtonPanel);
/*     */ 
/*  93 */     this.statusPanel = new JPanel(new GridBagLayout());
/*  94 */     this.statusLabel = new JLabel("Spravne: " + correct + " Nespravne: " + wrong);
/*  95 */     this.statusPanel.add(this.statusLabel);
/*  96 */     this.bottomPanel.add(this.statusPanel);
/*     */ 
/*  98 */     setLayout(new BorderLayout());
/*  99 */     add(this.questionTop, "North");
/* 100 */     add(this.questionOptions, "Center");
/* 101 */     add(this.bottomPanel, "South");
/* 102 */     this.nextButton.addActionListener(this);
			  this.correctButton.addActionListener(this);
/*     */   }
/*     */ 
/*     */   public void actionPerformed(ActionEvent ae)
/*     */   {
			  if (ae.getActionCommand().equals("Correct")){

				  for (int i = 0; i < buttons.size(); i++)  {
					  if (((QuestionRadioButton)buttons.get(i)).isTrue() && ((QuestionRadioButton)buttons.get(i)).isSelected() || !((QuestionRadioButton)buttons.get(i)).isTrue() && !((QuestionRadioButton)buttons.get(i)).isSelected()){
						  ((QuestionRadioButton)buttons.get(i)).setForeground(Color.GREEN);   
				  	  } else {

				  		((QuestionRadioButton)buttons.get(i)).setForeground(Color.RED);
				  	  }
				  }			
				 doLayout();
				  revalidate();
				  repaint();
				  
			  }
			  
				
/* 107 */     if (ae.getActionCommand().equals("Next"))
/* 108 */       if (counter == this.questions.size() - 1) {
/* 109 */         if (this.selected) {
/* 110 */           correct += 1;
/*     */         }
/*     */         else {
/* 113 */           wrong += 1;
/*     */         }
/* 115 */         this.statusLabel.setText("Spravne: " + correct + " Nespravne: " + 
/* 116 */           wrong);
/* 117 */         Object[] buttons = { "Znovu", "Koniec" };
/*     */ 
/* 120 */         int rc = JOptionPane.showOptionDialog(null, "Spravne " + correct + " Nespravne " + wrong, "Confirmation", 
/* 121 */           2, 0, null, buttons, buttons[0]);
/* 122 */         System.out.println(rc);
/*     */ 
/* 124 */         if (rc == 1) {
/* 125 */           System.exit(0);
/* 126 */         } else if (rc == 0)
/*     */         {
/* 128 */           counter = 0;
/* 129 */           correct = 0;
/* 130 */           wrong = 0;
/* 131 */           this.statusLabel.setText("Spravne: " + correct + " Nespravne: " + 
/* 132 */             wrong);
/* 133 */           this.questionTop.setBorder(
/* 134 */             BorderFactory.createTitledBorder(((Question)this.questions.get(counter))
/* 135 */             .getQuestionNumber()));
/* 136 */           this.questionText.setText(((Question)this.questions.get(counter)).getQuestionText());
/* 137 */           this.questionOptions.removeAll();
/* 138 */           checkBoxButtonPanel(counter);
/* 139 */           this.questionOptions.revalidate();
/* 140 */           this.questionOptions.repaint();
/* 141 */           this.nextButton.setEnabled(false);
/* 142 */         } else if (rc == -1) {
/* 143 */           System.exit(0);
/*     */         }
/*     */       } else {
/* 146 */         counter += 1;
/* 147 */         this.questionTop.setBorder(
/* 148 */           BorderFactory.createTitledBorder(((Question)this.questions.get(counter))
/* 149 */           .getQuestionNumber()));
/* 150 */         this.questionText.setText(((Question)this.questions.get(counter)).getQuestionText());
/* 151 */         this.questionOptions.removeAll();

                  double trueCount = 0; 
                  String nespravnaOznacena = "NIE";
                  for (int i = 0; i < buttons.size(); i++)  {
                      if (((QuestionRadioButton)buttons.get(i)).isTrue() && ((QuestionRadioButton)buttons.get(i)).isSelected()){
                          trueCount ++;
                      } 
                      if (!((QuestionRadioButton)buttons.get(i)).isTrue() && ((QuestionRadioButton)buttons.get(i)).isSelected()){
                          nespravnaOznacena = "ANO";
                      }                       
                  
                  }
/* 156 */         if (trueCount > 0) {
/* 157 */           correct += 1;
                    correctInLastQuestion = trueCount/pocetSpravnych;
/*     */         }
/*     */         else {
                    correctInLastQuestion = 0;
/* 160 */           wrong += 1;
/*     */         }
/* 152 */         checkBoxButtonPanel(counter);
/* 153 */         this.questionOptions.revalidate();
/* 154 */         this.questionOptions.repaint();
/* 155 */         this.nextButton.setEnabled(false);
/* 162 */         this.statusLabel.setText("Spr: " + correct + " Nespr: " + 
/* 163 */           wrong + " body " + correctInLastQuestion + " nespOzn:" + nespravnaOznacena);
/* 164 */         repaint();
/*     */       }
/*     */   }
/*     */ 

    List buttons;
    double pocetSpravnych = 0;
/*     */   public void checkBoxButtonPanel(int counter)
/*     */   {
/* 183 */     List questionOptionsList = ((Question)this.questions.get(counter)).getQuestionOptions();
/* 185 */     Collections.shuffle(questionOptionsList);
/* 186 */     buttons = new ArrayList();
/*     */     QuestionRadioButton radioButton;
              for (int i = 0; i < questionOptionsList.size(); i++){  
/* 187 */     //for (QuestionText textOption : questionOptionsList) {
/* 188 */       radioButton = new QuestionRadioButton(((QuestionText)questionOptionsList.get(i)).getQuestionText(), ((QuestionText)questionOptionsList.get(i)).isTrue());
/* 190 */       buttons.add(radioButton);
/* 191 */       radioButton.addActionListener(this.sliceActionListener);
/*     */     }
/* 193 */     ButtonGroup buttonGroup = new ButtonGroup();
              pocetSpravnych = 0;
/*     */       
              for (int i = 0; i < buttons.size(); i++){
/* 195 */     //for (JRadioButton button : buttons) {
/* 196 */       //buttonGroup.add((QuestionRadioButton)buttons.get(i));
/* 197 */       this.questionOptions.add((QuestionRadioButton)buttons.get(i));
                if (((QuestionRadioButton)buttons.get(i)).isTrue())
                    pocetSpravnych++;
/*     */     }
              
/*     */   }
/*     */ 
/*     */   public static void main(String[] args)
/*     */     throws HeadlessException, ParserConfigurationException, SAXException, IOException
/*     */   {
/* 204 */     POPTrenazer mainFrame = new POPTrenazer("Test");
/* 205 */     mainFrame.setSize(640, 350);
/* 206 */     mainFrame.setLocationRelativeTo(null);
/* 207 */     mainFrame.setVisible(true);
/* 208 */     mainFrame.setDefaultCloseOperation(3);
/*     */   }
/*     */ }

/* Location:           /media/Data/Skola/5.rocnik/LS/PaOP/trenazer/kamsv.jar
 * Qualified Name:     sk.tuke.student.kamsv.tester.MainFrame
 * JD-Core Version:    0.6.2
 */