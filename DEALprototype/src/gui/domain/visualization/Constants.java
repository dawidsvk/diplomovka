package gui.domain.visualization;

import java.awt.FontMetrics;
import java.util.Hashtable;
import java.util.Map;

import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxStylesheet;

public class Constants {

	/**
	 * �rka udajovej bunky v pixeloch
	 */
	public static final int VERTEX_WIDTH = 80;
	
	/**
	 * v��ka �dajovej bunky v pixeloch
	 */
	public static final int VERTEX_HEIGHT = 25;

}
