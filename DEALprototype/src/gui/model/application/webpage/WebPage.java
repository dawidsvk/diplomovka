package gui.model.application.webpage;

public class WebPage {
	private String title;
	
	public WebPage(String title) {
		this.title = title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return this.title;
	}
}
