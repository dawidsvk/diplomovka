package gui.tools.listener;

import gui.model.application.events.UiEvent;

public interface UiEventListener {

	public void uiEventRecorded(UiEvent uiEvent);

}
