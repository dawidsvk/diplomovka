package gui.domain.visualization;

import gui.domain.visualization.elements.GraphCell;
import gui.domain.visualization.layout.DEALLayout;
import gui.domain.visualization.layout.DEALTreeLayout;
import gui.editor.DomainModelEditor;
import gui.editor.tree.TreeNode;
import gui.model.domain.ComponentInfoType;
import gui.model.domain.Term;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;

import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxPerimeter;
import com.mxgraph.view.mxStylesheet;

public class GraphVisualization {

	private DEALGraph graph;
	private int id;

	private ArrayList<Term> rootTerms = new ArrayList<Term>();

	public GraphVisualization() {
		graph = new DEALGraph(getStylesheet());
		graph.setLabelsClipped(true);
		id = -1; // cisluje od 0, na zaciatku je id++ (dajak to nefunguje) bug v
					// kniznici?
		// vertexes = new Hashtable<Term, Object>();
		insertData();
		layoutGraph();
	}

	public DEALGraph getGraph() {
		return graph;
	}

	public mxStylesheet getStylesheet() {

		Map<String, Object> vertexStyle = new Hashtable<String, Object>();

		// style.put(mxConstants.STYLE_FILLCOLOR, "#CACACA");
		vertexStyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
		vertexStyle.put(mxConstants.STYLE_PERIMETER, mxPerimeter.RectanglePerimeter);
		vertexStyle.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
		vertexStyle.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
		vertexStyle.put(mxConstants.STYLE_FILLCOLOR, "#616161");
		vertexStyle.put(mxConstants.STYLE_STROKECOLOR, "#212121");
		vertexStyle.put(mxConstants.STYLE_GRADIENTCOLOR, "#878787");
		vertexStyle.put(mxConstants.STYLE_GRADIENT_DIRECTION, mxConstants.DIRECTION_NORTH);

		vertexStyle.put(mxConstants.STYLE_FONTCOLOR, "#151515");

		Map<String, Object> edgeStyle = new Hashtable<String, Object>();

		edgeStyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CONNECTOR);
		edgeStyle.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_CLASSIC);
		edgeStyle.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
		edgeStyle.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
		edgeStyle.put(mxConstants.STYLE_STROKECOLOR, "#212121");
		edgeStyle.put(mxConstants.STYLE_FONTCOLOR, "#151515");

		mxStylesheet stylesheet = new mxStylesheet();
		stylesheet.setDefaultVertexStyle(vertexStyle);
		stylesheet.setDefaultEdgeStyle(edgeStyle);
		return stylesheet;
	}

	public void setEditMode(boolean value) {
		graph.setCellsMovable(value);
		graph.setCellsResizable(value);
		graph.setCellsEditable(value);
	}

	private void insertData() {
		graph.getModel().beginUpdate();
		try {
			makeGraph();
		} finally {
			graph.getModel().endUpdate();
		}
	}

	private void layoutGraph() {
		// mxCompactTreeLayout layout = new mxCompactTreeLayout(graph);
		// DEALTreeLayout layout = new DEALTreeLayout(graph);
		DEALLayout layout = new DEALLayout(graph);
		Object cell = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		try {
			layout.execute(cell, rootTerms, 4, false);
			checkHidden();
		} finally {
			graph.getModel().endUpdate();
			graph.refresh();
		}
	}
	
	public void resetLayout(){
		
	}

	private void makeGraph() {
		TreeModel model = DomainModelEditor.getInstance().getDomainTree().getModel();
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) model.getRoot();
		Object obj = rootNode.getUserObject();
		Object root = insertVertex(rootNode.toString(), null);
		mxCell rootCell = (mxCell) root;
		rootCell.setVisible(false);
		if (obj instanceof Term) {
			// GraphData.getInstance().addVertex((Term) obj, root);
			// vertexes.put((Term) obj, root);
		}
		makeGraphChilds(rootNode, root, true);
	}

	private Object insertVertex(String name, Term t) {
		id++;
		Object vertx = graph.createVertex(graph.getDefaultParent(), Integer.toString(id), name, 0, 0, GraphHelper.VERTEX_WIDTH, GraphHelper.VERTEX_HEIGHT, null);
		return graph.addCell(vertx);
	}

	private Object insertEdge(Object from, Object to) {
		return graph.insertEdge(graph.getDefaultParent(), null, "", from, to);
	}

	private void makeGraphChilds(DefaultMutableTreeNode parent, Object parentGraphNode, boolean bRoot) {
		int iChildren = parent.getChildCount();
		if (bRoot) {
			for (int i = 0; i < parent.getChildCount(); i++) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) parent.getChildAt(i);
				rootTerms.add((Term) node.getUserObject());
			}
		}
		for (int i = 0; i < iChildren; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) parent.getChildAt(i);
			Object obj = node.getUserObject();
			Object childGraphNode = insertVertex(node.toString(), (Term) obj);
			if (!bRoot) {
				Object edge = insertEdge(parentGraphNode, childGraphNode);
				GraphCell gCell = new GraphCell((mxCell) childGraphNode, (mxCell) edge);
				gCell.setHidden(((TreeNode) parent.getChildAt(i)).isHidden());
				GraphData.getInstance().addVertex((Term) obj, gCell);
			} else {
				GraphCell gCell = new GraphCell((mxCell) childGraphNode, null);
				gCell.setHidden(((TreeNode) parent.getChildAt(i)).isHidden());
				GraphData.getInstance().addVertex((Term) obj, gCell);
			}
			makeGraphChilds(node, childGraphNode, false);
		}
	}

	private void checkHidden() {
		for (Map.Entry<Term, GraphCell> entry : GraphData.getInstance().getVertexes().entrySet()) {
			if (entry.getValue().isHidden()) {
				entry.getValue().getCell().setStyle("opacity=20");
				if (entry.getValue().hasEdge()) {
					entry.getValue().getEdge().setStyle("opacity=20");
				}
			}
		}
	}

	public void updateGraphVertexColor(boolean bShow) {
		graph.getModel().beginUpdate();
		try {
			Enumeration<Term> enumKey = GraphData.getInstance().getVertexes().keys();
			while (enumKey.hasMoreElements()) {
				Term t = enumKey.nextElement();
				Object cell = GraphData.getInstance().getVertexes().get(t);
				GraphCell mxCell = (GraphCell) cell;
				if (bShow) {
					ComponentInfoType infoType = t.getComponentInfoType();
					if (infoType != null)
						switch (infoType) {
						case TEXTUAL:
						case DESCRIPTIVE:
							mxCell.getCell().setStyle("fillColor=blue;gradientColor=#1209c2");
							break;
						case FUNCTIONAL:
							mxCell.getCell().setStyle("fillColor=red;gradientColor=#ae1919");
							break;
						case CONTAINERS:
							mxCell.getCell().setStyle("fillColor=green");
							break;
						case LOGICALLY_GROUPING:
							mxCell.getCell().setStyle("fillColor=cyan");
							break;
						case CUSTOM:
							mxCell.getCell().setStyle("fillColor=yellow");
							break;
						case UNKNOWN:
							mxCell.getCell().setStyle("fillColor=while");
							break;
						default:
							mxCell.getCell().setStyle(null);
							break;
						}
				} else {
					mxCell.getCell().setStyle(null);
				}

			}
		} finally {
			graph.getModel().endUpdate();
			graph.refresh();
		}
	}

}
