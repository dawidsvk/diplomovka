package gui.domain.visualization;

import gui.domain.visualization.elements.GraphCell;
import gui.domain.visualization.elements.Indicator;
import gui.domain.visualization.gui.GuiControlPanel;
import gui.editor.DomainModelEditor;
import gui.model.domain.Term;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.handler.mxCellMarker;
import com.mxgraph.util.mxConstants;

public class GraphMouseListener extends mxCellMarker implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;

	private mxGraphComponent graphComponent;
	private VisualizationPanel vizPanel;

	private static final int TYPE_VERTEX = 0;
	private static final int TYPE_EDGE = 1;
	private static final int TYPE_OTHER = 2;

	private GraphCell selectedCell;

	public GraphMouseListener(mxGraphComponent graphComponent, VisualizationPanel vizPanel, Color color) {
		super(graphComponent, color);
		this.graphComponent = graphComponent;
		this.vizPanel = vizPanel;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		GraphHelper.log("mouse clicked");
		if (e.getClickCount() == 1) {
			handleOneClick(e);
		} else if (e.getClickCount() == 2) {
			handleDoubleClick(e);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		GraphHelper.log("mouse pressed");
		handlePopupMenu(e);
		if (controlPanel.isStateSearch()) {
			controlPanel.setStateSearch(false);
			vizPanel.getGraphVisualization().endSearchState();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		GraphHelper.log("mouse released");
		handlePopupMenu(e);
		reset();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		// GraphHelper.log("mouse entered");
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		// GraphHelper.log("mouse exited");

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		// GraphHelper.log("time: " + e.getWhen());
		process(e);
	}

	private void setOpacity(GraphCell cell, boolean hide, boolean children) {
		if (!hide) {
			cell.getCell().setStyle("opacity=100");
			if (cell.hasEdge())
				cell.getEdge().setStyle("opacity=100");
		} else {
			cell.getCell().setStyle("opacity=20");
			if (cell.hasEdge())
				cell.getEdge().setStyle("opacity=20");
		}

		cell.setHidden(hide);

		if (children) {
			Term t = GraphData.getInstance().getTermByCell(cell);
			for (int i = 0; i < t.getChildrenCount(); i++) {
				setOpacity(GraphData.getInstance().getVertexes().get(t.getChildAt(i)), hide, true);
			}
		}
	}

	private void handlePopupMenu(MouseEvent e) {
		if (e.isPopupTrigger()) {

			int type = getComponentType(e);

			if (type == TYPE_VERTEX) {
				for (final Map.Entry<Term, GraphCell> entry : GraphData.getInstance().getVertexes().entrySet()) {
					if (entry.getValue() == selectedCell) {
						JPopupMenu menu = new JPopupMenu("Show/Hide");
						JMenuItem item, item2;
						if (entry.getValue().isHidden()) {
							item = new JMenuItem("Show");
							item2 = new JMenuItem("Show All");
						} else {
							item = new JMenuItem("Hide");
							item2 = new JMenuItem("Hide All");
						}
						item.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								graphComponent.getGraph().getModel().beginUpdate();
								try {
									setOpacity(entry.getValue(), !entry.getValue().isHidden(), false);

								} finally {
									graphComponent.getGraph().refresh();
									graphComponent.getGraph().getModel().endUpdate();
								}
							}
						});

						item2.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								graphComponent.getGraph().getModel().beginUpdate();
								try {
									setOpacity(entry.getValue(), !entry.getValue().isHidden(), true);

								} finally {
									graphComponent.getGraph().refresh();
									graphComponent.getGraph().getModel().endUpdate();
								}

							}
						});
						menu.add(item);
						menu.add(item2);
						menu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

		}
	}

	private void handleDoubleClick(MouseEvent e) {
		int type = getComponentType(e);

		if (type == TYPE_VERTEX) {
			for (Map.Entry<Term, GraphCell> entry : GraphData.getInstance().getVertexes().entrySet()) {
				if (entry.getValue() == selectedCell) {
					System.out.println(entry.getKey().getName());
					GraphHelper.log("dasdas: " + entry.getKey().getChildrenCount());
					mxCell firstChild;
					if (entry.getKey().hasChildren()) {
						firstChild = GraphData.getInstance().getVertexes().get(entry.getKey().getChildAt(0)).getCell();
					} else {
						return;
					}
					showHide(entry.getKey().getChildren(), !firstChild.isVisible());
					System.out.println("isvisible " + firstChild.isVisible());
					GraphCell actCell = entry.getValue();
					if (!firstChild.isVisible()) {
						// UKAZ TROJUHOLNIK
						if (GraphData.getInstance().hasIndicator(actCell)) {
							graphComponent.getGraph().getModel().beginUpdate();
							try {
								GraphData.getInstance().getCellIndicator(actCell).getCell().setVisible(true);
							} finally {
								graphComponent.getGraph().refresh();
								graphComponent.getGraph().getModel().endUpdate();
							}
						} else {
							graphComponent.getGraph().getModel().beginUpdate();
							try {
								Indicator indi = new Indicator(entry);
								mxCell indicator = indi.getIndicatorAsCell(graphComponent);
								GraphCell gCell = new GraphCell(indicator);
								gCell.setIndicator(true);
								GraphData.getInstance().addIndicator(actCell, gCell);
							} finally {
								graphComponent.getGraph().refresh();
								graphComponent.getGraph().getModel().endUpdate();
							}
						}
					} else {
						// HIDE TROJUHOLNIK
						graphComponent.getGraph().getModel().beginUpdate();
						try {
							GraphData.getInstance().getCellIndicator(actCell).getCell().setVisible(false);
						} finally {
							graphComponent.getGraph().refresh();
							graphComponent.getGraph().getModel().endUpdate();
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param childrens
	 * @return if show - true or hide - false
	 */
	private void showHide(List<Term> childrens, boolean visibility) {

		graphComponent.getGraph().getModel().beginUpdate();
		try {
			for (Term t : childrens) {
				GraphCell gCell = (GraphCell) GraphData.getInstance().getVertexes().get(t);
				mxCell mxCell = gCell.getCell();
				mxCell.setVisible(visibility);
				// sky vsetky child indikatory
				if (GraphData.getInstance().hasIndicator(gCell)) {
					GraphData.getInstance().getCellIndicator(gCell).getCell().setVisible(false);
				}
				if (t.hasChildren()) {
					showHide(t.getChildren(), visibility);
				}
			}
		} finally {
			graphComponent.getGraph().getModel().endUpdate();
			graphComponent.getGraph().refresh();
		}
	}

	private void handleOneClick(MouseEvent e) {
		int type = getComponentType(e);

		if (type == TYPE_VERTEX) {
			// update lavy panel
			for (Map.Entry<Term, GraphCell> entry : GraphData.getInstance().getVertexes().entrySet()) {
				if (entry.getValue() == selectedCell) {
					System.out.println(entry.getKey().getName());

					DomainModelEditor.getInstance().showInTrees(entry.getKey());
				}
			}

		} else if (type == TYPE_EDGE) {
			// onEdge click
			// uvidime co tu budeme robit, ci vobec nieco
		}
	}

	// toto treba lepsie spravit, potrebujem vratit aj cell
	private int getComponentType(MouseEvent e) {

		Object cell = graphComponent.getCellAt(e.getX(), e.getY());
		if (cell != null && cell instanceof mxCell) {
			if (((mxCell) cell).isVertex()) {
				selectedCell = GraphData.getInstance().getCellbyMxCell((mxCell) cell);
				return TYPE_VERTEX;
			} else if (((mxCell) cell).isEdge()) {
				return TYPE_EDGE;
			} else {
				return TYPE_OTHER;
			}
		}
		return TYPE_OTHER;
	}

}
