package gui.domain.visualization;

import gui.domain.visualization.gui.GuiControlPanel;
import gui.domain.visualization.gui.GuiProgress;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import com.mxgraph.swing.mxGraphComponent;

public class VisualizationPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private mxGraphComponent graphComponent;
	private GraphVisualization vizGraph;
	
	private boolean isInited = false;
	
	public VisualizationPanel() {
		super();
		isInited = false;
	}

	public void initializeView(final boolean isShowInfoTypeCheckboxSelected) {
		GraphHelper.log("START: " + System.currentTimeMillis()/1000);
		final GuiProgress guiProgress = new GuiProgress(this);
		guiProgress.show(null);
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				setLayout(new GridBagLayout());

				vizGraph = new GraphVisualization();
				
				if(isShowInfoTypeCheckboxSelected)
					actionPerformedInfoTypeCheckBox(isShowInfoTypeCheckboxSelected);
				
				graphComponent = new mxGraphComponent(vizGraph.getGraph());
				graphComponent.setPreferredSize(new Dimension(getWidth(), getHeight()));
				GraphMouseListener gml = new GraphMouseListener(graphComponent, new Color(0x00D89840));
				graphComponent.getGraphControl().addMouseListener(gml);
				graphComponent.getGraphControl().addMouseMotionListener(gml);
				graphComponent.setDragEnabled(false);
				graphComponent.setGridVisible(true);
				graphComponent.setToolTips(true);
				
				GridBagConstraints gbc = new GridBagConstraints();
				
				gbc.gridx = 0;
				gbc.gridy = 0;
				gbc.fill = gbc.BOTH;
				add(graphComponent, gbc);
				GuiControlPanel controlPanel = new GuiControlPanel(VisualizationPanel.this, graphComponent);
				controlPanel.show(gbc);
				
				isInited = true;
				GraphHelper.log("ENDT: " + System.currentTimeMillis()/1000);
				guiProgress.destroy();
			}
		});
		thread.start();
				
	}
	
	public GraphVisualization getGraphVisualization(){
		return vizGraph;
	}
	
	public void actionPerformedInfoTypeCheckBox(boolean isChecked){
		vizGraph.updateGraphVertexColor(isChecked);
	}
	
	public boolean isInited(){
		return isInited;
	}
	
	
	
}
