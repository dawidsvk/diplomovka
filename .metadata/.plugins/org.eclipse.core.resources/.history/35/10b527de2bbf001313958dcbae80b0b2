package gui.domain.visualization;

import gui.domain.visualization.gui.GuiControlPanel;
import gui.domain.visualization.gui.GuiProgress;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import com.mxgraph.swing.mxGraphComponent;

public class VisualizationPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private mxGraphComponent graphComponent;
	private GraphVisualization vizGraph;
	
	private boolean isInited = false;
	
	public VisualizationPanel() {
		super();
		isInited = false;
	}

	public void initializeView(final boolean isShowInfoTypeCheckboxSelected) {
		GraphHelper.log("START: " + System.currentTimeMillis()/1000);
		final GuiProgress guiProgress = new GuiProgress(this);
		guiProgress.show();
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				setLayout(new BoxLayout(VisualizationPanel.this, BoxLayout.Y_AXIS));

				vizGraph = new GraphVisualization();
				
				if(isShowInfoTypeCheckboxSelected)
					actionPerformedInfoTypeCheckBox(isShowInfoTypeCheckboxSelected);
				
				graphComponent = new mxGraphComponent(vizGraph.getGraph());
				graphComponent.setPreferredSize(new Dimension(getWidth(), getHeight()));
				
				GraphMouseListener gml = new GraphMouseListener(graphComponent, new Color(0x00D89840));
				graphComponent.getGraphControl().addMouseListener(gml);
				graphComponent.getGraphControl().addMouseMotionListener(gml);
				graphComponent.setDragEnabled(false);
				graphComponent.setGridVisible(true);
				graphComponent.setToolTips(true);
				
				add(graphComponent);
				
				add(Box.createRigidArea(new Dimension(0,5)));
				GuiControlPanel controlPanel = new GuiControlPanel(VisualizationPanel.this, graphComponent);
				controlPanel.show();
				
				isInited = true;
				GraphHelper.log("ENDT: " + System.currentTimeMillis()/1000);
				guiProgress.destroy();
				invalidate();
				updateUI();
			}
		});
		thread.start();
				
	}
	
	public GraphVisualization getGraphVisualization(){
		return vizGraph;
	}
	
	public void actionPerformedInfoTypeCheckBox(boolean isChecked){
		vizGraph.updateGraphVertexColor(isChecked);
	}
	
	public boolean isInited(){
		return isInited;
	}
	
	
	
}
